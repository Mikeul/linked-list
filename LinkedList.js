class Node{
  constructor(element){
    this.element = element;
    this.nextPointer = null;
  }
}

class LinkedList{
  constructor(){
    this.head = null;
    this.tail = null;
    this.size = 0;
  }

  add(element){
    const newNode = new Node(element);
    if(!this.head) {
      this.head = newNode
      this.tail = newNode
    }
    else{
      this.tail.nextPointer = newNode;
      this.tail = newNode;
    }
    this.size++;
  }

  insertAt(element, index){
    if(index > 0 && index > this.size) return false;
    else{
      const newNode = new Node(element);
      let previousNode;
      if(index === 0){
        node.nextPointer = this.head;
        this.head = newNode;
      }
      else{
        let currentNode = this.head;
        let i = 0;
        while(i < index){
          i++;
          previousNode = currentNode;
          currentNode = currentNode.nextPointer;
        }
        newNode.nextPointer = currentNode;
        previousNode.nextPointer = newNode
      }
      this.size++;
    }
  }

  removeFrom(index){
    if(index > 0 && index > this.size) return - 1;
    else{
      let currentNode = this.head;
      let previousNode = currentNode
      let i = 0;
      if(index === 0) this.head = currentNode.nextPointer;
      else{
        while(i < index){
          i++;
          previousNode = currentNode;
          currentNode = currentNode.nextPointer;
        }
        previousNode.nextPointer = currentNode.nextPointer;
      }
      this.size--;
    }
  }

  removeElement(element){
    let currentNode = this.head;
    let previousNode = null;
    while(currentNode){
      if(currentNode.element === element){
        if(!previousNode) this.head = currentNode.nextPointer;
        else previousNode.nextPointer = currentNode.nextPointer;
        this.size--;
        return currentNode.element;
      }
      previousNode = currentNode;
      currentNode = currentNode.nextPointer;
    }
    return -1;
  }

  indexOf(element){
    let count = 0;
    let currentNode = this.head;

    while(currentNode){
      if(currentNode.element === element) return count;
      count++;
      currentNode = currentNode.nextPointer;
    }
    return -1;
  }

  isEmpty(){
    return this.size === 0;
  }

  sizeOfList(){
    return this.size;
  }

  getList(){
    let currentNode = this.head;
    let str = '';
    while(currentNode){
      str += `${currentNode.element} ${currentNode.nextPointer? "->" : "-> null"} `;
      currentNode = currentNode.nextPointer;
    }
    return str
  }
}

module.exports = LinkedList;